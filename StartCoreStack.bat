﻿#http://ss64.com/nt/start.html

cd %~p0

echo Start Solr
START /B /DSolr\Server\4.4.0\ StartSolr.bat

#echo Start Redis Master
#START /B /DRedis\Server\3.2.100\64\ redis-server.exe --port 6379

#echo Start Redis Slave
#START /B /DRedis\Server\3.2.100\64\ redis-server.exe --port 6380 --slaveof 127.0.0.1 6379

#echo Start Mongo
#START /B /DMongoDB\Server\mongodb-win32-x86_64-2008plus-ssl-3.0.15\ StartMongo.bat

pause