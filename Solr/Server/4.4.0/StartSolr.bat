IF EXIST solr-4.4.0\example\solr\localhost GOTO HASDIR

ECHO 'Creating localhost'
XCOPY solr-4.4.0\example\solr\collection1 solr-4.4.0\example\solr\localhost /S /E /I
copy core.properties solr-4.4.0\example\solr\localhost\core.properties /Y

ECHO 'Creating Links'

del solr-4.4.0\example\solr\solr.xml
del solr-4.4.0\example\solr\localhost\conf\schema.xml
del solr-4.4.0\example\solr\localhost\conf\solrconfig.xml
del solr-4.4.0\example\solr\localhost\conf\dbdataconfig.xml

mklink /h solr-4.4.0\example\solr\solr.xml ..\..\..\..\core.platform\code\EZ.Store.Mvc.Web\Config\Solr\solr.xml 
mklink /h solr-4.4.0\example\solr\localhost\conf\schema.xml ..\..\..\..\core.platform\code\EZ.Store.Mvc.Web\Config\Solr\schema.xml
mklink /h solr-4.4.0\example\solr\localhost\conf\solrconfig.xml ..\..\..\..\core.platform\code\EZ.Store.Mvc.Web\Config\Solr\solrconfig.xml
mklink /h solr-4.4.0\example\solr\localhost\conf\dbdataconfig.xml ..\..\..\..\core.platform\code\EZ.Store.Mvc.Web\Config\Solr\dbdataconfig.xml 

pause
:HASDIR

copy jetty.xml solr-4.4.0\example\etc\jetty.xml /Y
cd solr-4.4.0\example
java -jar start.jar 

pause